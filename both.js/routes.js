Router.route('/', function () {
    this.layout('ApplicationLayout');
    this.render('hello');
}, {
    name: "home"
});

Router.route('/post/:_id', function () {
    this.layout('ApplicationLayout');
    this.render('post', {
        data: function () {
            return Posts.findOne({_id: this.params._id});
        }
    });
});