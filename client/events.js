Template.ApplicationLayout.onCreated(function (){
    Meteor.subscribe('posts');
});

Template.hello.events({
    "submit .name-form": function (event) {
        event.preventDefault();
        var name = event.target.firstname.value;
        
        Posts.insert({
            name: name,
            username: Meteor.user().username,
        });
    }
});
Template.post.events({
    "click .posts-class": function() {
    Router.go('home');
    }
});